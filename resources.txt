engine:	canvas-cpu:			https://www.sitepoint.com/building-3d-engine-javascript/
		canvas-gpu:			https://webglfundamentals.org/webgl/lessons/webgl-load-obj.html
lib:						pending... C Wtc 42
object:						pending... Mohale Wtc Bbd Wesbank Matheos
os:							kalilo Wtc

javascript:					https://www.w3schools.com/jsref/
rotations:					https://en.wikipedia.org/wiki/Rotation_matrix
lines:						https://tutorial.math.lamar.edu/classes/calciii/eqnsoflines.aspx
							http://mathcentral.uregina.ca/QQ/database/QQ.09.01/murray2.html

collision detection:		http://www.jeffreythompson.org/collision-detection/line-point.php
ray-triangle intersection	http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/

canvas-overlay:				https://stackoverflow.com/questions/3008635/html5-canvas-element-multiple-layers

vbs.sendkeys:				https://ss64.com/vb/sendkeys.html
ps1.add-type				https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/add-type?view=powershell-7

blender text:				https://gamefromscratch.com/creating-text-in-blender/
blender	triangulate:		https://blender.stackexchange.com/questions/13727/how-do-you-un-triangulate-a-mesh

video texture:				https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Manipulating_video_using_canvas
image pixelation:			https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas


c#:
cursorpos:					https://norbertvajda.wordpress.com/2018/12/29/get-the-cursor-position-with-powershell/
mouseevent:					https://stackoverflow.com/questions/39353073/how-i-can-send-mouse-click-in-powershell

backend:
python flask fetch api		https://pythonise.com/series/learning-flask/flask-and-fetch-api
php xmlhttp (deprecated)	https://stackoverflow.com/questions/7071544/post-from-xmlhttp-with-parameters

videos, canvas				https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Manipulating_video_using_canvas


keyhook multiple keys 		https://stackoverflow.com/questions/5203407/how-to-detect-if-multiple-keys-are-pressed-at-once-using-javascript

Trigonometry				https://www.mathsisfun.com/algebra/trig-finding-angle-right-triangle.html
