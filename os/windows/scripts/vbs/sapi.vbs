Dim l_sapi, l_voice


Set l_sapi = WScript.CreateObject("SAPI.SpVoice")

If WScript.Arguments.Item(0) = "-f" Then
	l_voice = 1
Else
	l_voice = 0
End If

Set l_sapi.Voice = l_sapi.GetVoices.Item(l_voice)

l_sapi.Rate = 2

l_sapi.Speak(WScript.Arguments.Item(l_voice))
