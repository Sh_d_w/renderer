$src = @"

using System;
using System.Drawing;			//Bitmap

using System.Drawing.Imaging;	//PixelFormat
using System.Windows.Forms;		//screen


using System.Runtime.InteropServices;	//mouse stuff


public class screen_events
{
//https://msdn.microsoft.com/en-us/library/windows/desktop/ms646270(v=vs.85).aspx
[StructLayout(LayoutKind.Sequential)]
struct INPUT
{ 
    public int        type; // 0 = INPUT_MOUSE,
                            // 1 = INPUT_KEYBOARD
                            // 2 = INPUT_HARDWARE
    public MOUSEINPUT mi;
}

//https://msdn.microsoft.com/en-us/library/windows/desktop/ms646273(v=vs.85).aspx
[StructLayout(LayoutKind.Sequential)]
struct MOUSEINPUT
{
    public int    dx ;
    public int    dy ;
    public int    mouseData ;
    public int    dwFlags;
    public int    time;
    public IntPtr dwExtraInfo;
}

//This covers most use cases although complex mice may have additional buttons
//There are additional constants you can use for those cases, see the msdn page
const int MOUSEEVENTF_MOVED      = 0x0001 ;
const int MOUSEEVENTF_LEFTDOWN   = 0x0002 ;
const int MOUSEEVENTF_LEFTUP     = 0x0004 ;
const int MOUSEEVENTF_RIGHTDOWN  = 0x0008 ;
const int MOUSEEVENTF_RIGHTUP    = 0x0010 ;
const int MOUSEEVENTF_MIDDLEDOWN = 0x0020 ;
const int MOUSEEVENTF_MIDDLEUP   = 0x0040 ;
const int MOUSEEVENTF_WHEEL      = 0x0080 ;
const int MOUSEEVENTF_XDOWN      = 0x0100 ;
const int MOUSEEVENTF_XUP        = 0x0200 ;
const int MOUSEEVENTF_ABSOLUTE   = 0x8000 ;

const int screen_length = 0x10000 ;

//https://msdn.microsoft.com/en-us/library/windows/desktop/ms646310(v=vs.85).aspx
[System.Runtime.InteropServices.DllImport("user32.dll")]
extern static uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

	public static void image_comp(string p_filename)
	{
			int		i, j, x, y;
			int		ib, ib2;
			int		sw, sh, iw, ih;

			Bitmap		l_image;
			Bitmap		l_screen;

			i = 0;
		//load image pixels
			l_image = new Bitmap(p_filename);		//grape.png

		//get screen pixels

			l_screen = new Bitmap(1920, 1080, PixelFormat.Format32bppArgb);

			Rectangle captureRectangle = Screen.AllScreens[0].Bounds;		
			Graphics captureGraphics = Graphics.FromImage(l_screen);
			captureGraphics.CopyFromScreen(captureRectangle.Left,captureRectangle.Top,0,0,captureRectangle.Size);

			// Color pixel = l_screen.GetPixel(0, 0);
			// Console.WriteLine("RGB: " + pixel.R + " " + pixel.G);

			//loop here
			//	l_screen.Save(@".\CaptureNEW.jpg", ImageFormat.Jpeg); 	

		//loop through large image

			sw = l_screen.Width;
			sh = l_screen.Height;
			iw = l_image.Width;
			ih = l_image.Height;

			Color[,] ascreen = new Color[sw, sh];
			Color[,] aimage = new Color[iw, ih];

			x = 0;
			while (x < sw) {
				y = 0;
				while (y < sh) {
					//screen.pixel:
					ascreen[x, y] = l_screen.GetPixel(x, y);
					y++;
				}
				x++;
			}
			x = 0;
			while (x < iw) {
				y = 0;
				while (y < ih) {
					//image.pixel:
					aimage[x, y] = l_image.GetPixel(x, y);
					y++;
				}
				x++;
			}

			ib = 0;
			i = 0;
			while (i < sw) {
				j = 0;
				while (j < sh) {
					ib2 = 1;		//image loop true
					x = 0;
					while (x < iw) {
						y = 0;
						while (y < ih && ib2 == 1) {
									ib = 0; 	//screen loop true							
								if (i + x < sw && j + y < sh) {
									if (ascreen[i + x, j + y].R == aimage[x, y].R)
										ib += 1;
									if (ascreen[i + x, j + y].G == aimage[x, y].G)
										ib += 1;
									if (ascreen[i + x, j + y].B == aimage[x, y].B)
										ib += 1;
									if (ascreen[i + x, j + y].A == aimage[x, y].A)
										ib += 1;
								}
								else
									ib2 = 0;	// stop image loop 
								y++;
								if (ib < 4)		// if pixels don't match
									ib2 = 0;	//		stop image loop
								if (ib2 == 0){	//if stop image loop
									ib = 0;		//then no longer a match (within looped constraints)
									break;
								}
						}
						x++;
						if (ib2 == 0)		//if stop image loop
							break;
					}
					j++;
					if (ib == 4) {	//if still match after image loop
						//found
						Console.WriteLine("FOUND\n " + i + " " + j);

						//Move the mouse
						INPUT[] input = new INPUT[3];
						input[0].mi.dx = (i + (iw / 2)) * (65535/System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width);
						input[0].mi.dy = (j + (ih / 2)) * (65535/System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height);
						input[0].mi.dwFlags = MOUSEEVENTF_MOVED | MOUSEEVENTF_ABSOLUTE;
						//Left mouse button down
						input[1].mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
						//Left mouse button up
						input[2].mi.dwFlags = MOUSEEVENTF_LEFTUP;
						SendInput(3, input, Marshal.SizeOf(input[0]));

						break;

					}
				}
				i++;
				if (ib == 4) //if still match after image loop
					break;
			}

			if (ib != 4)
				Console.WriteLine("END loop: NOT FOUND" + " | " + aimage[0, 0].G);
	}
}
"@

Add-Type -TypeDefinition $src -ReferencedAssemblies System.Windows.Forms,System.Drawing

[screen_events]::image_comp("grape.png");
