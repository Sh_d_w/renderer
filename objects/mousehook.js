var	mousehook = new object_("", 0);

// Initialize the movement
function initMove(evt) {
	mousedown = true;
	mx = evt.clientX;
	my = evt.clientY;
}

//object.mousehook:
function	move(evt) {
	if (mousedown) {
		for (let i = 0; i < 1; i++) {
			// objects[i].mousehook[0]((evt.clientX - mx), (evt.clientY - my));
			objects[i].angle[0] += (evt.clientX - mx) * Math.PI / 360;
			objects[i].angle[1] += (evt.clientY - my) * Math.PI / 360;//180;

			objects[i].angle[0] = cube.angle[0] % 360;
			objects[i].angle[1] = cube.angle[1] % 360;
		}
		mx = evt.clientX;
		my = evt.clientY;
	}
}

function stopMove() {
	mousedown = false;
}

//function autorotate() {
//	for (var i = 0; i < 8; ++i)
//		rotatexyz(cube.vertices[i], cube_center, -Math.PI / 720, Math.PI / 720, 0);
//	render.render[0](objects, ctx, dx, dy);
//	autorotate_timeout = setTimeout(autorotate, 30);
//}
