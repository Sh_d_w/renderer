var render = new object_("", 0);

render.vertex = [ 0.0, 0.0, 0.0 ];

render.render = [];
render.construct = [];
render.algorithm = [];
render.render = [];
render.destruct;

render.construct[0] = function() {
}

render.algorithm[0] = function() {
}

render.render[0] = function(objects, ctx, dx, dy) {

	// For each object
	for (var i = 0, n_obj = objects.length; i < n_obj; ++i) {
		// For each face
		for (var j = 0, n_faces = objects[i].faces.length; j < n_faces; ++j) {
			// Current face
			var face = objects[i].faces[j];

			// Draw the first vertex
			var P = project_cpu(objects[i], face[0]);
			ctx.beginPath();
			ctx.moveTo(P[0] + dx, -P[1] + dy);

			// Draw the other vertices
			for (var k = 1, n_vertices = face.length; k < n_vertices; ++k) {
				P = project_cpu(objects[i], face[k]);
				ctx.lineTo(P[0] + dx, -P[1] + dy);
			}

			// Close the path and draw the face
			ctx.strokeStyle = 'rgba(' + objects[i].color[0] + ', ' + objects[i].color[1] + ', ' + objects[i].color[2] + ', ' + objects[i].color[3] + ')';
			ctx.fillStyle = 'rgba(' + objects[i].color[0] + ', ' + objects[i].color[1] + ', ' + objects[i].color[2] + ', ' + objects[i].color[3] + ')';

			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	}
}

// GPU:
function	degToRad(deg) {
	return deg * Math.PI / 180;
}

//async
render.render[1] = function(p_object, p_bufferinfo, time) {
// function	render_gpu(time) {
	time *= 0.001;  // convert to seconds

	webglUtils.resizeCanvasToDisplaySize(gl.canvas);
	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
	gl.enable(gl.DEPTH_TEST);
	//gl.enable(gl.CULL_FACE);

	let fieldOfViewRadians = degToRad(60);
	let aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
	let projection = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

	let up = [0, 1, 0];

	// Compute the camera's matrix using look at.
	let camera = m4.lookAt(cameraPosition, cameraTarget, up);

	// Make a view matrix from the camera matrix.
	let view = m4.inverse(camera);

	let sharedUniforms = {
		u_lightDirection: m4.normalize([-1, 3, 5]),
		u_view: view,
		u_projection: projection,
	};

	gl.useProgram(meshProgramInfo.program);

	// calls gl.uniform
	webglUtils.setUniforms(meshProgramInfo, sharedUniforms);

	// calls gl.bindBuffer, gl.enableVertexAttribArray, gl.vertexAttribPointer
	webglUtils.setBuffersAndAttributes(gl, meshProgramInfo, p_object.gpu[1/*buffer*/] );

	// calls gl.uniform
	webglUtils.setUniforms(meshProgramInfo, {
		u_world: m4.project_matrix(p_object, time),//m4.axisRotation([1, 1, 0], time),//m4.xRotation(time),
		u_diffuse: [p_object.color[0] / 255, p_object.color[1] / 255, p_object.color[2] / 255, p_object.color[3]],
	});

	// calls gl.drawArrays or gl.drawElements
	webglUtils.drawBufferInfo(gl, p_object.gpu[1/*buffer*/] );
}

render.destruct = function() {
}