// function	gpu_faces() {
	// this.faces;
	// this.buffer;
// }

function	project_cpu(p_object, M) {
		let		l_scale_offset;
		let		l_translate = [ 0.0, 0.0, 0.0 ];
		// let		l_scale = [ p_object.scale[0], p_object.scale[1], p_object.scale[2] ]
		let		l_relative = [ p_object.center[0], p_object.center[1], p_object.center[2] ];

		l_scale_offset = 200;

	//pixel calculations:
		// todo: / 100 if perspective:
		render.vertex[0] = M[0];
		render.vertex[1] = M[1];
		render.vertex[2] = M[2];

		if (p_object.lhs) {
			//relative:
				l_translate[0] = p_object.lhs.translate[0];
				l_translate[1] = p_object.lhs.translate[1];
				l_translate[2] = p_object.lhs.translate[2];

				// l_scale[0] = p_object.lhs.scale[0];
				// l_scale[1] = p_object.lhs.scale[1];
				// l_scale[2] = p_object.lhs.scale[2];

				rotatexyz(l_relative, object.origin, p_object.lhs.angle[0], p_object.lhs.angle[1], p_object.lhs.angle[2]);
			//rotates:
				rotatexyz(render.vertex, object.origin, p_object.lhs.angle[0] + p_object.angle[0], p_object.lhs.angle[1] + p_object.angle[1], p_object.lhs.angle[2] + p_object.angle[2]);
		}
		else
			rotatexyz(render.vertex, object.origin, p_object.angle[0], p_object.angle[1], p_object.angle[2]);

		l_translate[0] += l_relative[0] + p_object.translate[0];
		l_translate[1] += l_relative[1] + p_object.translate[1];
		l_translate[2] += l_relative[2] + p_object.translate[2];

		render.vertex[0] = (render.vertex[0] * (p_object.scale[0])) + (l_translate[0]);
		render.vertex[1] = (render.vertex[1] * (p_object.scale[1])) + (l_translate[1]);
		render.vertex[2] = (render.vertex[2] * (p_object.scale[2])) + (l_translate[2]);

	// orthographic
		//return [ render.vertex[0], render.vertex[2] ];
	// perspective
		// Distance between the camera and the plane
		let d = 200;
		let r = d / render.vertex[1];

		return [ r * render.vertex[0], r * render.vertex[2] ];
}

function	project_gpu(p_object, angleInRadians, dst) {
		let		l_translate = [0.0, 0.0, 0.0];
		let		l_relative = [ p_object.center[0], p_object.center[2], p_object.center[1] ];

		dst = dst || new MatType(16);

		if (p_object.lhs) {
			//relative:
				l_translate[0] = p_object.lhs.translate[0];
				l_translate[1] = p_object.lhs.translate[1];
				l_translate[2] = p_object.lhs.translate[2];

				rotatexyz(l_relative, object.origin, p_object.lhs.angle[1], p_object.lhs.angle[0], p_object.lhs.angle[2]);

			//rotates:
				multiply( xRotation(p_object.lhs.angle[1], dst), yRotation(p_object.lhs.angle[0]), dst)
				multiply(dst, zRotation(p_object.lhs.angle[2]), dst)
		}
		else {
		//rotate (own orbit)
			multiply( xRotation(p_object.angle[1], dst), yRotation(p_object.angle[0]), dst)
			multiply(dst, zRotation(p_object.angle[2]), dst)
		}

		//scale:
			multiply(dst, scaling(p_object.scale[0], p_object.scale[1], p_object.scale[2]), dst)

		//translate:
			dst[12] = -p_object.translate[0] + l_translate[0] + l_relative[0];
			dst[13] = -p_object.translate[2] + l_translate[2] + l_relative[1];
			dst[14] = p_object.translate[1] + l_translate[1] + l_relative[2];

		//offset rotation:
			translate(dst, 0, 0, 0, dst)
		return dst;
}

function	straight_line(l_vertice, l_v1, l_v2, l_time) {
	if (l_v1[0] !== l_v2[0])
		l_vertice[0] = l_v1[0] + l_v2[0] * l_time;
	if (l_v1[1] !== l_v2[1])
		l_vertice[1] = l_v1[1] + l_v2[1] * l_time;
	if (l_v1[2] !== l_v2[2])
		l_vertice[2] = l_v1[2] + l_v2[2] * l_time;
}

// Rotate a vertice
function rotatexyz(M, center, beta, theta, phi) {
	// Rotation matrix coefficients
	let		ct, st;
	let		x;
	let		y;
	let		z;

	//x
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(beta);
	st = Math.sin(beta);
	M[0] = ct * x - st * y;
	M[1] = st * x + ct * y;

	//y
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(theta);
	st = Math.sin(theta);
	M[1] = ct * y - st * z;
	M[2] = st * y + ct * z;

	//z
	x = M[0];
	y = M[1];
	z = M[2];
	ct = Math.cos(phi);
	st = Math.sin(phi);
	M[0] = ct * x - st * z;
	M[2] = st * x + ct * z;
}
