var	cube = new object_("", 0);

cube.state;
cube.keyhook = [];
cube.mousehook = [];

cube.construct = [];
cube.algorithm = [];
cube.render = [];
cube.destruct;

cube.construct[0] = function() {
	object.construct[1](cube);

	cube.state = 0;
	object.generate[0](files.cube, cube, object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);

	cube.rhs = [];
	cube.rhs[0] = new object_("", 0);
	object.construct[1](cube.rhs[0]);

	cube.rhs[0].lhs = cube;
	cube.rhs[0].scale = [0.1, 0.1, 0.1];
	// cube.rhs[0].center = [1.0, 1.0, 1.0];
	cube.rhs[0].color = [250, 150, 0, 0.3];
	object.generate[0](files.cube, cube.rhs[0], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
}
	var	uy = 0.0, ux = 0.0;
	var	v = 6, w = v + 1;

cube.algorithm[0] = function() {
	//MAP:// SEUDO:
	let	l_x = 0.5, l_y = 1.0;
	let	v1 = 2, v2 = v1 + 1;
	let	v3 = 6, v4 = v3 + 1;

	let	v5 = [[], []];

	// uy += 0.01;
	// if (uy > 2.0)
		// uy = 0.0;

	uy += 0.01;
	if (uy > 2.0) {
		uy = 0.0;
	
		ux += 0.1;
		if (ux > 2.0)
			ux = 0.0;
	}
//Straight line:
	// cube.rhs[0].center[0] = cube.vertices[v][0];
	// cube.rhs[0].center[1] = cube.vertices[v][1];
	// cube.rhs[0].center[2] = cube.vertices[v][2];

	// straight_line(cube.rhs[0].center, cube.vertices[v], cube.vertices[w], uy);

//MAP:// SEUDO:

	//vertice 1 for X-axis:
		// v5[0][0] = cube.vertices[v1][0] - cube.vertices[v2][0] * x;
		// v5[0][1] = cube.vertices[v1][1] - cube.vertices[v2][1] * x;
		// v5[0][2] = cube.vertices[v1][2] - cube.vertices[v2][2] * x;

		v5[0][0] = cube.vertices[v1][0];
		v5[0][1] = cube.vertices[v1][1];
		v5[0][2] = cube.vertices[v1][2];

		straight_line(v5[0], cube.vertices[v1], cube.vertices[v2], ux);

	//vertice 2 for X-axis:
		// v5[1][0] = cube.vertices[v3][0] - cube.vertices[v4][0] * x;
		// v5[1][1] = cube.vertices[v3][1] - cube.vertices[v4][1] * x;
		// v5[1][2] = cube.vertices[v3][2] - cube.vertices[v4][2] * x;

		v5[1][0] = cube.vertices[v3][0];
		v5[1][1] = cube.vertices[v3][1];
		v5[1][2] = cube.vertices[v3][2];

		straight_line(v5[1], cube.vertices[v3], cube.vertices[v4], ux);

	//vertices 1 and 2 formed from X-axis for Y-axis:
		// cube.rhs[0].center[0] = v5[0][0] - v5[1][0] * y;
		// cube.rhs[0].center[1] = v5[0][1] - v5[1][1] * y;
		// cube.rhs[0].center[2] = v5[0][2] - v5[1][2] * y;
		// straight_line(cube.rhs[0].center, v5[0], v5[1], l_y);

	//test:
		// cube.rhs[0].center[0] = v5[0][0];
		// cube.rhs[0].center[1] = v5[0][1];
		// cube.rhs[0].center[2] = v5[0][2];
	
	cube.rhs[0].center[0] = v5[0][0];
	cube.rhs[0].center[1] = v5[0][1];
	cube.rhs[0].center[2] = v5[0][2];

	straight_line(cube.rhs[0].center, v5[0], v5[1], uy);


	//kehooks for MAPping:
		// (x and y need keyhooks)
}

cube.render[0] = function(time) {
//CPU:
	if (cube.state === 0 || cube.state === 2) {
		render.render[0](objects, ctx, dx, dy);

		//to controllably 
		render.render[0]([ cube.rhs[0] ], ctx, dx, dy);
	}
//GPU:
	if (cube.state === 1 || cube.state === 2) {
		cube.render[1](cube, time);

		cube.render[1](cube.rhs[0], time);
	}
}

cube.render[1] = function(p_object, time) {
//GPU:
	render.render[1](p_object, time);
}

cube.keyhook[0] = function() {
	let		l_speed = 0.1;
	//translate here
	if (keyhook.keycode === 37)//LE
		cube.translate[0] -= l_speed;
	if (keyhook.keycode === 39)//RI
		cube.translate[0] += l_speed;

	if (keyhook.keycode === 38)//UP
		cube.translate[2] += l_speed;
	if (keyhook.keycode === 12)//5 DO
		cube.translate[2] -= l_speed;

	if (keyhook.keycode === 85)//U
		cube.translate[1] -= l_speed;
	if (keyhook.keycode === 74)//5 J
		cube.translate[1] += l_speed;
//  65A  87//W  83//S 68//D

}

cube.mousehook[0] = function(p_dx, p_dy) {
	// object.mousehook[0](p_object, evt);
	let		theta = (p_dx) * Math.PI / 360;
	let		phi = (p_dy) * Math.PI / 180;

	//object.mousehook[0](cube, evt);

	//let i = mx;
	//do on meta data instead:
//		rotatexyz(cube.vertices[0], object.origin, theta, phi, 0);

	// for (let j = 0; j < 8; ++j) {
		// rotatexyz(cube.vertices[j], object.origin, theta, phi, 0);
	// }
}
