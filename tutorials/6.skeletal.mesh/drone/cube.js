var	cube = new object_("", 0);

cube.state;
cube.keyhook = [];
cube.mousehook = [];

cube.generate = [];
cube.construct = [];
cube.algorithm = [];
cube.render = [];
cube.destruct;

cube.construct[0] = function() {
	object.construct[1](cube);

	cube.state = 0;
	object.generate[0](files.cube, cube, object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);

	cube.rhs = [];
	cube.rhs[0] = new object_("", 0);
	object.construct[1](cube.rhs[0]);

	// cube.rhs[0].lhs = cube;
	// cube.rhs[0].scale = [0.025, 0.025, 0.025];
	// // cube.rhs[0].center = [1.0, 1.0, 1.0];
	// cube.rhs[0].color = [250, 150, 0, 0.3];
	// object.generate[0](files.torso/*head*/, cube.rhs[0], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
	//head:
	cube.generate[0](0, files.torso, [250, 150, 0, 0.3], cube, [0.025, 0.025, 0.025]);
	//chest:
	cube.generate[0](1, files.torso, [0, 250, 0, 0.3], cube, [0.05, 0.05, 0.05]);
	//torso:
	// cube.generate[0](14, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	// cube.generate[0](15, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	//leg 1:
	cube.generate[0](2, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](3, files.torso, [150, 150, 150, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](4, files.torso, [50, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	//leg 2:
	cube.generate[0](5, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](6, files.torso, [150, 150, 150, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](7, files.torso, [50, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);

	//arm 1:
	cube.generate[0](8, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](9, files.torso, [150, 150, 150, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](10, files.torso, [50, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	//arm 2:
	cube.generate[0](11, files.torso, [250, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](12, files.torso, [150, 150, 150, 0.3], cube, [0.025, 0.025, 0.025]);
	cube.generate[0](13, files.torso, [50, 250, 250, 0.3], cube, [0.025, 0.025, 0.025]);



	// cube.rhs[1] = new object_("", 0);
	// object.construct[1](cube.rhs[1]);
	// cube.rhs[1].lhs = cube;1
	// cube.rhs[1].scale = [0.05, 0.05, 0.05];
	// cube.rhs[1].color = [0, 250, 0, 0.3];
	// object.generate[0](files.torso /*chest*/, cube.rhs[1], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
	// cube.rhs[1].faces[0][0][1] = -3.0;
	// cube.rhs[1].faces[0][3][1] = -3.0;
	// cube.rhs[1].faces[0][1][1] = 3.0;
	// cube.rhs[1].faces[0][2][1] = 3.0;

	// cube.rhs[2] = new object_("", 0);
	// object.construct[1](cube.rhs[2]);
	// cube.rhs[2].lhs = cube;
	// cube.rhs[2].scale = [0.025, 0.025, 0.025];
	// // cube.rhs[2].angle = [90, 90, 90];
	// cube.rhs[2].color = [250, 250, 250, 0.3];
	// object.generate[0](files.torso, cube.rhs[2], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
	// cube.rhs[2].faces[0][0][0] = 0.0;
	// cube.rhs[2].faces[0][3][0] = 0.0;
	// cube.rhs[2].faces[0][1][0] = 0.0;
	// cube.rhs[2].faces[0][2][0] = 0.0;
	//
	// cube.rhs[2].faces[0][0][2] = -3.0;
	// cube.rhs[2].faces[0][3][2] = 1.0;
	// cube.rhs[2].faces[0][1][2] = -3.0;
	// cube.rhs[2].faces[0][2][2] = 1.0;
	//
	// cube.rhs[2].faces[0][0][1] = 0.5;
	// cube.rhs[2].faces[0][3][1] = 0.5;
	// cube.rhs[2].faces[0][1][1] = -0.5;
	// cube.rhs[2].faces[0][2][1] = -0.5;


}
	var	uy = 0.0, ux = 0.0;
	var	v = 6, w = v + 1;

cube.algorithm[0] = function() {
	//MAP:// SEUDO:
	let	l_x = 0.5, l_y = 1.0;
	let	v1 = 2, v2 = v1 + 1;
	let	v3 = 6, v4 = v3 + 1;

	let	v5 = [[], []];

	// uy += 0.01;
	// if (uy > 2.0)
		// uy = 0.0;

	// uy += 0.01;
	// if (uy > 2.0) {
		// uy = 0.0;
	
		// ux += 0.1;
		// if (ux > 2.0)
			// ux = 0.0;
	// }
	
//Straight line:
	// cube.rhs[0].center[0] = cube.vertices[v][0];
	// cube.rhs[0].center[1] = cube.vertices[v][1];
	// cube.rhs[0].center[2] = cube.vertices[v][2];

	// straight_line(cube.rhs[0].center, cube.vertices[v], cube.vertices[w], uy);

//MAP:// SEUDO:

	//vertice 1 for X-axis:
		// v5[0][0] = cube.vertices[v1][0] - cube.vertices[v2][0] * x;
		// v5[0][1] = cube.vertices[v1][1] - cube.vertices[v2][1] * x;
		// v5[0][2] = cube.vertices[v1][2] - cube.vertices[v2][2] * x;

		v5[0][0] = cube.vertices[v1][0];
		v5[0][1] = cube.vertices[v1][1];
		v5[0][2] = cube.vertices[v1][2];

		straight_line(v5[0], cube.vertices[v1], cube.vertices[v2], ux);

	//vertice 2 for X-axis:
		// v5[1][0] = cube.vertices[v3][0] - cube.vertices[v4][0] * x;
		// v5[1][1] = cube.vertices[v3][1] - cube.vertices[v4][1] * x;
		// v5[1][2] = cube.vertices[v3][2] - cube.vertices[v4][2] * x;

		v5[1][0] = cube.vertices[v3][0];
		v5[1][1] = cube.vertices[v3][1];
		v5[1][2] = cube.vertices[v3][2];

		straight_line(v5[1], cube.vertices[v3], cube.vertices[v4], ux);

	//vertices 1 and 2 formed from X-axis for Y-axis:
		// cube.rhs[0].center[0] = v5[0][0] - v5[1][0] * y;
		// cube.rhs[0].center[1] = v5[0][1] - v5[1][1] * y;
		// cube.rhs[0].center[2] = v5[0][2] - v5[1][2] * y;
		// straight_line(cube.rhs[0].center, v5[0], v5[1], l_y);

	//test:
		// cube.rhs[0].center[0] = v5[0][0];
		// cube.rhs[0].center[1] = v5[0][1];
		// cube.rhs[0].center[2] = v5[0][2];
	
	cube.rhs[0].center[0] = v5[0][0];
	cube.rhs[0].center[1] = v5[0][1];
	cube.rhs[0].center[2] = v5[0][2] + 0.1;

	straight_line(cube.rhs[0].center, v5[0], v5[1], uy);




	//kehooks for MAPping:
		// (x and y need keyhooks)

//chest
	cube.rhs[1].center[0] = cube.rhs[0].center[0];
	cube.rhs[1].center[1] = cube.rhs[0].center[1];
	cube.rhs[1].center[2] = cube.rhs[0].center[2];

// //torso 1
	// cube.rhs[2].center[0] = cube.rhs[0].center[0];
	// cube.rhs[2].center[1] = cube.rhs[0].center[1];
	// cube.rhs[2].center[2] = cube.rhs[0].center[2] - 0.17;//+0.85
// //torso 2
	// cube.rhs[3].center[0] = cube.rhs[0].center[0];
	// cube.rhs[3].center[1] = cube.rhs[0].center[1];
	// cube.rhs[3].center[2] = cube.rhs[0].center[2] - 0.25;//+0.8

//leg 1	0
	cube.rhs[2].center[0] = cube.rhs[0].center[0] - 0.05;
	cube.rhs[2].center[1] = cube.rhs[0].center[1] + 0.05;
	cube.rhs[2].center[2] = cube.rhs[0].center[2];//+0.5
//leg 1	1
	cube.rhs[3].center[0] = cube.rhs[0].center[0] - 0.1;
	cube.rhs[3].center[1] = cube.rhs[0].center[1] + 0.1;
	cube.rhs[3].center[2] = cube.rhs[0].center[2];//+0.8
//leg 1	2
	cube.rhs[4].center[0] = cube.rhs[0].center[0] - 0.15;
	cube.rhs[4].center[1] = cube.rhs[0].center[1] + 0.15;
	cube.rhs[4].center[2] = cube.rhs[0].center[2];//+0.8

//leg 2 0
	cube.rhs[5].center[0] = cube.rhs[0].center[0] + 0.05;
	cube.rhs[5].center[1] = cube.rhs[0].center[1] + 0.05;
	cube.rhs[5].center[2] = cube.rhs[0].center[2];//+0.8
//leg 2 1
	cube.rhs[6].center[0] = cube.rhs[0].center[0] + 0.1;
	cube.rhs[6].center[1] = cube.rhs[0].center[1] + 0.1;
	cube.rhs[6].center[2] = cube.rhs[0].center[2];//+0.8
//leg 2 2
	cube.rhs[7].center[0] = cube.rhs[0].center[0] + 0.15;
	cube.rhs[7].center[1] = cube.rhs[0].center[1] + 0.15;
	cube.rhs[7].center[2] = cube.rhs[0].center[2];//+0.8

//arm 1 0
	cube.rhs[8].center[0] = cube.rhs[0].center[0] - 0.05;
	cube.rhs[8].center[1] = cube.rhs[0].center[1] - 0.05;
	cube.rhs[8].center[2] = cube.rhs[0].center[2];
//arm 1 1
	cube.rhs[9].center[0] = cube.rhs[0].center[0] - 0.1;
	cube.rhs[9].center[1] = cube.rhs[0].center[1] - 0.1;
	cube.rhs[9].center[2] = cube.rhs[0].center[2];
//arm 1 2
	cube.rhs[10].center[0] = cube.rhs[0].center[0] - 0.15;
	cube.rhs[10].center[1] = cube.rhs[0].center[1] - 0.15;
	cube.rhs[10].center[2] = cube.rhs[0].center[2];

//arm 2 0
	cube.rhs[11].center[0] = cube.rhs[0].center[0] + 0.05;
	cube.rhs[11].center[1] = cube.rhs[0].center[1] - 0.05;
	cube.rhs[11].center[2] = cube.rhs[0].center[2];//+0.8
//arm 2 1
	cube.rhs[12].center[0] = cube.rhs[0].center[0] + 0.1;
	cube.rhs[12].center[1] = cube.rhs[0].center[1] - 0.1;
	cube.rhs[12].center[2] = cube.rhs[0].center[2];//+0.8
//arm 2 2
	cube.rhs[13].center[0] = cube.rhs[0].center[0] + 0.15;
	cube.rhs[13].center[1] = cube.rhs[0].center[1] - 0.15;
	cube.rhs[13].center[2] = cube.rhs[0].center[2];//+0.8



//head
	cube.rhs[0].center[0] = cube.rhs[0].center[0];
	cube.rhs[0].center[1] = cube.rhs[0].center[1] + 0.05;
	cube.rhs[0].center[2] = cube.rhs[0].center[2];


	// cube.rhs[0].angle[0] = (0.025 + cube.rhs[0].angle[0]) % 360;
	// cube.rhs[1].angle[0] = (0.025 + cube.rhs[1].angle[0]) % 360;
	// cube.rhs[2].angle[0] = (0.025 + cube.rhs[2].angle[0]) % 360;
	// cube.rhs[3].angle[0] = (0.025 + cube.rhs[3].angle[0]) % 360;
	// cube.rhs[4].angle[0] = (0.025 + cube.rhs[4].angle[0]) % 360;

}

cube.render[0] = function(time) {
//CPU:
	if (cube.state === 0 || cube.state === 2) {
		render.render[0](objects, ctx, dx, dy);

		//to controllably 
		for (let i = 0; i < cube.rhs.length; i++) 
			render.render[0]([ cube.rhs[i] ], ctx, dx, dy);
	//	render.render[0]([ cube.rhs[0] ], ctx, dx, dy);
	//	render.render[0]([ cube.rhs[1] ], ctx, dx, dy);
	//	render.render[0]([ cube.rhs[2] ], ctx, dx, dy);
	//	render.render[0]([ cube.rhs[3] ], ctx, dx, dy);

	}
//GPU:
	if (cube.state === 1 || cube.state === 2) {
		cube.render[1](cube, time);

		for (let i = 0; i < cube.rhs.length; i++) 
			cube.render[1](cube.rhs[i], time);
	//	cube.render[1](cube.rhs[0], time);
	//	cube.render[1](cube.rhs[1], time);
	//	cube.render[1](cube.rhs[2], time);
	//	cube.render[1](cube.rhs[3], time);
	}
}

cube.render[1] = function(p_object, time) {
//GPU:
	render.render[1](p_object, time);
}

cube.keyhook[0] = function() {
	let		l_speed = 0.1;
	//translate here
	if (keyhook.keycode === 37)//LE
		cube.translate[0] -= l_speed;
	if (keyhook.keycode === 39)//RI
		cube.translate[0] += l_speed;

	if (keyhook.keycode === 38)//UP
		cube.translate[2] += l_speed;
	if (keyhook.keycode === 12)//5 DO
		cube.translate[2] -= l_speed;

	if (keyhook.keycode === 85)//U
		cube.translate[1] -= l_speed;
	if (keyhook.keycode === 74)//5 J
		cube.translate[1] += l_speed;
//  65A  87//W  83//S 68//D

	if (keyhook.keycode === 65)//A
		uy += 0.02 * (uy < 2.0);
	if (keyhook.keycode === 68)//D
		uy -= 0.02 * (uy > 0.0);
	if (keyhook.keycode === 83)//S
		ux += 0.02 * (ux < 2.0);
	if (keyhook.keycode === 87)//W
		ux -= 0.02 * (ux > 0.0);


}

cube.mousehook[0] = function(p_dx, p_dy) {
	// object.mousehook[0](p_object, evt);
	let		theta = (p_dx) * Math.PI / 360;
	let		phi = (p_dy) * Math.PI / 180;

	//object.mousehook[0](cube, evt);

	//let i = mx;
	//do on meta data instead:
//		rotatexyz(cube.vertices[0], object.origin, theta, phi, 0);

	// for (let j = 0; j < 8; ++j) {
		// rotatexyz(cube.vertices[j], object.origin, theta, phi, 0);
	// }
}

cube.generate[0] = function(p_object_id, p_file, p_color, p_lhs, p_scale) {
	//cube.rhs[1]

	cube.rhs[p_object_id] = new object_("", 0);
	object.construct[1](cube.rhs[p_object_id]);
	cube.rhs[p_object_id].lhs = cube;
	cube.rhs[p_object_id].scale = p_scale;
	cube.rhs[p_object_id].color = p_color;
	object.generate[0](p_file /*chest*/, cube.rhs[p_object_id], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);

}

