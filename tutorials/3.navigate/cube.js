var	cube = new object_("", 0);

cube.state;
cube.keyhook = [];
cube.mousehook = [];

cube.construct = [];
cube.algorithm = [];
cube.render = [];
cube.destruct;

cube.construct[0] = function() {
	object.construct[1](cube);

	cube.state = 0;
	object.generate[0](files.cube, cube, object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);

	cube.rhs = [];
	cube.rhs[0] = new object_("", 0);
	object.construct[1](cube.rhs[0]);

	cube.rhs[0].lhs = cube;
	cube.rhs[0].scale = [0.1, 0.1, 0.1];
	// cube.rhs[0].center = [1.0, 1.0, 1.0];
	cube.rhs[0].color = [0, 150, 0, 0.3];
	object.generate[0](files.cube, cube.rhs[0], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
}
	var	ul = 0.0;
	var	v = 6, w = v + 1;

cube.algorithm[0] = function() {
	ul += 0.01;
	if (ul > 2.0)
		ul = 0.0;

	cube.rhs[0].center[0] = cube.vertices[v][0];
	cube.rhs[0].center[1] = cube.vertices[v][1];
	cube.rhs[0].center[2] = cube.vertices[v][2];

	// if (cube.vertices[v][0] !== cube.vertices[w][0])
		// cube.rhs[0].center[0] = cube.vertices[v][0] + cube.vertices[w][0] * ul;
	// if (cube.vertices[v][1] !== cube.vertices[w][1])
		// cube.rhs[0].center[1] = cube.vertices[v][1] + cube.vertices[w][1] * ul;
	// if (cube.vertices[v][2] !== cube.vertices[w][2])
		// cube.rhs[0].center[2] = cube.vertices[v][2] + cube.vertices[w][2] * ul;

	straight_line(cube.rhs[0].center, cube.vertices[v], cube.vertices[w], ul);


	// cube.rhs[0].center[0] = 1 + 1 * 0;
	// cube.rhs[0].center[1] = -1 + 1 * ul;
	// cube.rhs[0].center[2] = 1 + -1 * ul;

	// cube.rhs[0].center[0] = 1;
	// cube.rhs[0].center[1] = 1;
	// cube.rhs[0].center[2] = 1;

	// cube.rhs[0].center[0] = 1;
	// cube.rhs[0].center[1] = 1;
	// cube.rhs[0].center[2] = -1;


	// cube.rhs[0].center[0] = tx[0] + ty[0] * t;
	// cube.rhs[0].center[1] = tx[1] + ty[1] * t;
	// cube.rhs[0].center[2] = tx[2] + ty[2] * t;

	// cube.rhs[0].center[0] = cube.vertices[v][0];
	// cube.rhs[0].center[1] = cube.vertices[v][1] * t;
	// cube.rhs[0].center[2] = cube.vertices[v][2];

	// cube.rhs[0].center[0] = cube.vertices[w][0];
	// cube.rhs[0].center[1] = cube.vertices[w][1];
	// cube.rhs[0].center[2] = cube.vertices[w][2];

	//MAP:// SEUDO:
	//let	x = 0.5, y = 0.5;
	//let	v1 = 2, v2 = 3
	//let	v3 = 1, v4 = 2;

	//let	v5 = [[], []];

	//vertice 1 for X-axis:
		// v5[0][0] = cube.vertices[v1][0] - cube.vertices[v2][0] * x;
		// v5[0][1] = cube.vertices[v1][1] - cube.vertices[v2][1] * x;
		// v5[0][2] = cube.vertices[v1][2] - cube.vertices[v2][2] * x;

	//vertice 2 for X-axis:
		// v5[1][0] = cube.vertices[v3][0] - cube.vertices[v4][0] * x;
		// v5[1][1] = cube.vertices[v3][1] - cube.vertices[v4][1] * x;
		// v5[1][2] = cube.vertices[v3][2] - cube.vertices[v4][2] * x;

	//vertices 1 and 2 formed from X-axis for Y-axis:
		// cube.rhs[0].center[0] = cube.vertices[ v5[0][0] ][0] - cube.vertices[ v5[1][0] ][0] * y;
		// cube.rhs[0].center[0] = cube.vertices[ v5[0][1] ][1] - cube.vertices[ v5[1][1] ][1] * y;
		// cube.rhs[0].center[0] = cube.vertices[ v5[0][2] ][2] - cube.vertices[ v5[1][2] ][2] * y;

	//kehooks for MAPping:
		// (x and y need keyhooks)
}

cube.render[0] = function(time) {
//CPU:
	if (cube.state === 0 || cube.state === 2) {
		render.render[0](objects, ctx, dx, dy);

		//to controllably 
		render.render[0]([ cube.rhs[0] ], ctx, dx, dy);
	}
//GPU:
	if (cube.state === 1 || cube.state === 2) {
		cube.render[1](cube, time);

		cube.render[1](cube.rhs[0], time);

	}
}

cube.render[1] = function(p_object, time) {
//GPU:
	render.render[1](p_object, time);
}

cube.keyhook[0] = function() {
	let		l_speed = 0.1;
	//translate here
	if (keyhook.keycode === 37)//LE
		cube.translate[0] -= l_speed;
	if (keyhook.keycode === 39)//RI
		cube.translate[0] += l_speed;

	if (keyhook.keycode === 38)//UP
		cube.translate[2] += l_speed;
	if (keyhook.keycode === 12)//5 DO
		cube.translate[2] -= l_speed;

	if (keyhook.keycode === 85)//U
		cube.translate[1] -= l_speed;
	if (keyhook.keycode === 74)//5 J
		cube.translate[1] += l_speed;

}

cube.mousehook[0] = function(p_dx, p_dy) {
	// object.mousehook[0](p_object, evt);
	let		theta = (p_dx) * Math.PI / 360;
	let		phi = (p_dy) * Math.PI / 180;

	//object.mousehook[0](cube, evt);

	//let i = mx;
	//do on meta data instead:
//		rotatexyz(cube.vertices[0], object.origin, theta, phi, 0);

	// for (let j = 0; j < 8; ++j) {
		// rotatexyz(cube.vertices[j], object.origin, theta, phi, 0);
	// }
}
