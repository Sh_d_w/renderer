var	cube = new object_("", 0);

cube.state;
cube.keyhook = [];
cube.mousehook = [];

cube.construct = [];
cube.algorithm = [];
cube.render = [];
cube.destruct;

cube.construct[0] = function() {
	object.construct[1](cube);

	cube.state = 2;
	object.generate[0](files.cube, cube, object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);

	cube.rhs = [];
	cube.rhs[0] = new object_("", 0);
	object.construct[1](cube.rhs[0]);

	cube.rhs[0].lhs = cube;
	cube.rhs[0].scale = [0.1, 0.1, 0.1];
	cube.rhs[0].center = [1.0, 1.0, 0.0];
	cube.rhs[0].color = [0, 150, 0, 0.3];
	object.generate[0](files.cube, cube.rhs[0], object.origin, cube.state/* 0 CPU, 1 GPU, 2 CPU+GPU */);
}

cube.algorithm[0] = function() {
}

cube.render[0] = function(time) {
//CPU:
	if (cube.state === 0 || cube.state === 2) {
		render.render[0](objects, ctx, dx, dy);

		//to controllably 
		render.render[0]([ cube.rhs[0] ], ctx, dx, dy);
	}
//GPU:
	if (cube.state === 1 || cube.state === 2) {
		cube.render[1](cube, time);

		cube.render[1](cube.rhs[0], time);

	}
}

cube.render[1] = function(p_object, time) {
//GPU:
	render.render[1](p_object, time);
}

cube.keyhook[0] = function() {
	let		l_speed = 0.02;
	//translate here
	if (keyhook.keycode === 37)//L
		cube.translate[0] -= l_speed;
	if (keyhook.keycode === 39)//R
		cube.translate[0] += l_speed;

	if (keyhook.keycode === 38)//U
		cube.translate[2] += l_speed;
	if (keyhook.keycode === 12)//5 D
		cube.translate[2] -= l_speed;
}

cube.mousehook[0] = function(p_dx, p_dy) {
	// object.mousehook[0](p_object, evt);
	let		theta = (p_dx) * Math.PI / 360;
	let		phi = (p_dy) * Math.PI / 180;

	//object.mousehook[0](cube, evt);

	//let i = mx;
	//do on meta data instead:
//		rotatexyz(cube.vertices[0], object.origin, theta, phi, 0);

	// for (let j = 0; j < 8; ++j) {
		// rotatexyz(cube.vertices[j], object.origin, theta, phi, 0);
	// }
}
